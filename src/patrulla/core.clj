(ns patrulla.core)
(require '[clojure.data.csv :as csv]
         '[clojure.java.io :as io]
         '[clojure.string :as str])

(defn csv-data->maps [csv-data]
  (map zipmap
       (->> (first csv-data) ;; First row is the header
            (map keyword) ;; Drop if you want string keys instead
            repeat)
	  (rest csv-data)))

(defn nginx-routes []
  (with-open [reader (io/reader "nginx.csv")]
    (doall
      (csv-data->maps (csv/read-csv reader)))))

(defn re-ip [s]
  (re-find #"[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+" s))

(defn services-data
  ([services-file] (re-seq #"[^\n]+" (slurp services-file)))
  ([] (services-data "services.txt")))

(defn etc-hosts [services-file]
  (str/join "\n" (map #(str (re-ip %) "\t" (re-find #"[^ ]+" %))
                      (services-data services-file))))

(defn service [ip]
  (re-find #"[^ ]+"
           (first (filter #(str/includes? % ip)
                          (services-data)))))

(defn remove-trailing-diag [s]
  (if (and (= (last s) \/) (not (empty? (butlast s))))
    (str/join (butlast s))
    s))

(def data (map #(assoc % :ip (re-ip (:service-address %))
                       :route (remove-trailing-diag (:route %)))
               (nginx-routes)))
(def data2 (map #(assoc % :service (try (service (:ip %)) (catch Exception e)))
                data))

(def datos-para-ingress (filter :service data2))

(defn ingress-template [m]
  (str "
      - backend:
          serviceName: " (:service m) "
          servicePort: 80
        path: "(:route m)))

;;(pprint data2)
(comment
(with-open [writer (io/writer "out-file.csv")]
  (csv/write-csv writer (map vals data2)))

(defn out-file-csv [data]
  ))
